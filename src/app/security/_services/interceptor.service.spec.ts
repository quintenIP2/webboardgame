import {TestBed} from '@angular/core/testing';

import {HttpConfigInterceptor} from './interceptor.service';
import {Router, RouterModule} from '@angular/router';
import {LoginComponent} from '../login/login.component';
import {HomeComponent} from '../../main/home/home.component';
import {GameComponent} from '../../main/game/game.component';
import {AccountComponent} from '../account/account.component';
import {RegisterComponent} from '../register/register.component';
import {OverviewComponent} from '../../main/overview/overview.component';
import {AuthGuard} from '../_guards/auth.guard';
import {HttpClient, HttpHandler} from '@angular/common/http';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule, MatFormFieldModule, MatInputModule} from '@angular/material';
import {RouterTestingModule} from '@angular/router/testing';
import {appRoutes} from '../../app-routing.module';

describe('HttpConfigInterceptor', () => {
  TestBed.configureTestingModule({
    declarations: [LoginComponent,
      HomeComponent,
      GameComponent,
      AccountComponent,
      RegisterComponent,
      OverviewComponent], // actorviews in this test
    // providers: [AuthGuard, HttpClient, HttpHandler], // needed for authservice and authguard
    schemas: [CUSTOM_ELEMENTS_SCHEMA], // needed for board and game
    imports: [ReactiveFormsModule, BrowserAnimationsModule, MatInputModule, MatFormFieldModule, MatCardModule] // dependencies
  });
  // it('should be created', () => {
  //   const service: HttpConfigInterceptor = TestBed.get(HttpConfigInterceptor);
  //   expect(service).toBeTruthy();
  // });
});
