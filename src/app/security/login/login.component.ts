import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../_services/authentication.service';
import {AuthService as SocialAuthService, FacebookLoginProvider, GoogleLoginProvider, SocialUser} from 'ng4-social-login';
import {map} from 'rxjs/operators';


/**
 * component used to handle login requests of users
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  returnUrl: string;
  user: any = SocialUser;

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private authService: AuthenticationService,
              private socialAuthService: SocialAuthService) {
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  getMailErrors() {
    return this.form.email.hasError('required') ? 'You must enter an email address' :
      this.form.email.hasError('email') ? 'Not a valid email' : '';
  }

  getPassErrors() {
    return this.form.password.hasError('required') ? 'You must enter a pasword' : '';
  }

  get form() {
    return this.loginForm.controls;
  }

  // sends credentials to server and sends back a json object with a jwt inside
  login(): void {
    if (this.loginForm.valid) {
      console.log('form is valid');
      this.authService.tokenRequest(this.form.email.value, this.form.password.value)
        .pipe(map(value => {
          // this way the browser remembers which user is logged in
          // and thus the user stays logged in between page refreshes
          console.log('access_token fetched :' + value);
          localStorage.setItem('access_token', value['access_token']);
          localStorage.setItem('access_token_expiration', value['expires_in']);
          localStorage.setItem('logged_in', 'true');

          // navigate to the page you visited during an unauthenticated session or to homepage
          this.router.navigate([this.returnUrl]);

          // chain another observable to the tokenrequest observable
          // so that it can start this one when the token is fetched and stored
          return this.authService.getUserByEmail(this.form.email.value);
        })).subscribe(chainedUserObservable => {
        chainedUserObservable.subscribe(user => {
          console.log(user);
          localStorage.setItem('user_data', JSON.stringify(user));

          // tell all the subscribers that there is a new value
          this.authService.currentUserSubject.next(user);
          this.authService.usernameSubject.next(user.username);
        });
      });
    }
  }

  // external oauth token authentication
  private doTokenRequest(email: string, password: string): void {
    this.authService.tokenRequest(email, password)
      .subscribe(data => {
          console.log('access_token:' + JSON.stringify(data));
          localStorage.setItem('access_token', data.token);
          this.router.navigate([this.returnUrl]);
        }, err => console.log('error fetching token:' + JSON.stringify(err))
      );
  }

  public facebookLogin(): void {
    const socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        // returns user data from Facebook
        this.user = userData;
        this.doTokenRequest(this.user.email, this.user.password);
      }
    );
  }

  public googleLogin(): void {
    const socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        // returns user data from Google
        this.user = userData;
        this.doTokenRequest(this.user.email, this.user.password);
      }
    );
  }
}
