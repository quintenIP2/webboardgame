import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthenticationService} from '../_services/authentication.service';
import {environment} from '../../../environments/environment';

/**
 * grants access to certain pages based on the presence of a jwt token
 */

@Injectable({providedIn: 'root'})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthenticationService,
              private router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const token = localStorage.getItem(environment.localStorageToken);

    // not authenticated, so no access to other pages
    if (token === '' || token == null) {
      console.log('fetched token in authguard: ' + JSON.stringify(token));
      this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
      return false;
    }

    // true gives access to other pages
    return true;
  }
}
