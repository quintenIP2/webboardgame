import {async, inject, TestBed} from '@angular/core/testing';

import {AuthGuard} from './auth.guard';
import {RouterTestingModule} from '@angular/router/testing';
import {appRoutes} from '../../app-routing.module';
import {LoginComponent} from '../login/login.component';
import {HomeComponent} from '../../main/home/home.component';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule, MatFormFieldModule, MatInputModule} from '@angular/material';
import {GameComponent} from '../../main/game/game.component';
import {Router, RouterModule} from '@angular/router';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {HttpClient, HttpHandler} from '@angular/common/http';
import {AccountComponent} from '../account/account.component';
import {RegisterComponent} from '../register/register.component';
import {OverviewComponent} from '../../main/overview/overview.component';


describe('AuthGuard', () => {
  describe('canActivate', () => {

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [LoginComponent,
          HomeComponent,
          GameComponent,
          AccountComponent,
          RegisterComponent,
          OverviewComponent], // actorviews in this test
        providers: [AuthGuard, HttpClient, Router, HttpHandler], // needed for authservice and authguard
        schemas: [CUSTOM_ELEMENTS_SCHEMA], // needed for board and game
        imports: [ReactiveFormsModule, BrowserAnimationsModule, MatInputModule, RouterModule,
          MatFormFieldModule, MatCardModule, RouterTestingModule.withRoutes(appRoutes)] // dependencies
      });
      it('should be made', inject([AuthGuard], (guard: AuthGuard) => {
        expect(guard).toBeTruthy();
      }));

      it('should return true for a logged in user', async(inject([AuthGuard, Router], (guard: AuthGuard, router: Router) => {
        expect(guard.canActivate).toBeTruthy();
      })));

      it('should navigate to home for a logged out user', async(inject([AuthGuard, Router], (guard: AuthGuard, router: Router) => {
        spyOn(router, 'navigate');
        expect(guard.canActivate).toBeFalsy();
        expect(router.navigate).toHaveBeenCalled();
      })));
    });
  });
});
