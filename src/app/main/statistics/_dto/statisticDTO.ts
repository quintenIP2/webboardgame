import {UserDTO} from '../../../security/_dto/UserDTO';

export class StatisticDTO {
  player: UserDTO;
  gamesPlayed: number;
  gamesWon: number;
  avgScore: number;
  maxScore: number;
}
