export class Letter {
  char: string;
  lang: string;
  score: number;
  url: string;


  constructor(char: string, lang: string, url: string) {
    this.char = char;
    this.lang = lang;
    this.url = url;
  }
}
