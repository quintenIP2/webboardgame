export enum Multiplier {
  NONE,
  X2_LETTER,
  X3_LETTER,
  X2_WORD,
  X3_WORD
}
