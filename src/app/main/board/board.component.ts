import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Tile} from './_models/tile';
import {BoardService} from '../_services/board.service';
import {tileUrls} from '../../../environments/tiles';
import {Game} from '../game/_models/game';
import {Letter} from './_models/letter';
import {Cell} from './_models/cell';


/**
 * component to handle everything that has to do with a board
 */
@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {
  @Input() tiles;
  @Input() game: Game = new Game();
  @Input() cells: Cell[][] = [[]];
  @Input() lightStar: boolean;
  @Output() public wordEvent = new EventEmitter<any>();
  tileUrls = tileUrls;

  constructor(private boardService: BoardService) {
  }

  ngOnInit() {
  }

  /**
   * this method will receive an event(tile), x and y coordinates
   * the board child will emit this data to the game parent
   * @param event: tile object
   *
   * @param row: y
   * @param column: x
   * @author jonathan peers & glenn geysen
   */
  drop(event, row, column): void {
    event.preventDefault();

    const tileJson = event.dataTransfer.getData('tile');
    console.log(`tilejson ${tileJson}`);

    // parse and emit
    const t: Tile = JSON.parse(tileJson);
    const tile: Tile = new Tile();
    tile.x = column;
    tile.y = row;
    tile.letter = new Letter(t.letter.char, 'en_US', tileUrls[t.letter.char]);
    tile.letter.char = t.letter.char;
    tile.id = t.id;
    this.wordEvent.emit(tile);

    if (event.target.className === 'square') {
      const image = document.getElementById(tileJson);
      image.style.position = 'absolute';
      image.style.width = '45px';
      image.style.height = '45px';
      event.target.appendChild(image);
    }
  }

  draggedOver(event): void {
    event.preventDefault();
    event.target.style.backgroundColor = '#BABABA';
  }

  onDragLeave(event): void {
    event.target.style.backgroundColor = 'white';
  }
}
