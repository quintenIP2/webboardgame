import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {GameService} from '../_services/game.service';
import {map, tap} from 'rxjs/operators';
import {GameDto} from './_dto/game-dto';
import {Game} from './_models/game';
import {BehaviorSubject, Observable} from 'rxjs';
import {tileUrls} from '../../../environments/tiles';
import {Tile} from '../board/_models/tile';
import {Player} from './_models/player';
import {Cell} from '../board/_models/cell';
import {LaidDownTileDTO} from './_dto/laidDownTileDTO';
import {LetterRack} from './_models/letterrack';
import {LetterRackService} from '../_services/letter-rack.service';
import {Letter} from '../board/_models/letter';
import {LetterRackDto} from './_dto/letter-rack-dto';
import {environment} from '../../../environments/environment';
import {Score} from './_models/score';
import {PlayedTurnDto} from './_dto/played-turn-dto';
import {AuthenticationService} from '../../security/_services/authentication.service';
import {RxStompService} from '@stomp/ng2-stompjs';
import {StatisticService} from '../_services/statistic.service';


/**
 * component to handle everything that has to do with a game
 */
@Component({selector: 'app-game', templateUrl: './game.component.html', styleUrls: ['./game.component.scss']})
export class GameComponent implements OnInit {
  gameObservable: Observable<GameDto>;
  game: Game = new Game();

  cellsSubject = new BehaviorSubject<Cell[][]>([[]]);
  scoresSubject = new BehaviorSubject<Score[]>([]);
  currentlyPlayingSubject = new BehaviorSubject<number>(0);
  rackSubject = new BehaviorSubject<LetterRack>(new LetterRack(null, []));
  winnerSubject = new BehaviorSubject<String>('');

  players: Player[];
  tileUrls = tileUrls;
  placedTiles: Tile[] = [];

  // placetiles error
  error: string;
  private isoverSubject = new BehaviorSubject<boolean>(false);

  constructor(private activatedRoute: ActivatedRoute,
              private gameService: GameService,
              private rxStompService: RxStompService,
              private authService: AuthenticationService,
              private rackService: LetterRackService,
              private statisticService: StatisticService) {
  }

  ngOnInit() {
    // current game
    const id = this.activatedRoute.snapshot.paramMap.get('id');

    // fill myLetterRack with your current tiles
    this.rackService.getLetterRackByGameId(id).subscribe((r: LetterRackDto) => {
      const tiles = r.tiles.map(c => new Tile(
        new Letter(c.char, 'en_US', this.tileUrls[c.char]),
        null,
        0,
        0,
        c.id
      ));

      // update racksubject to update letterRack in html
      this.rackSubject.next(new LetterRack(
        new Player(r.player),
        tiles
      ));

      console.log(`(ngOnInit Letterrack fetch) to: ${[...this.rackSubject.getValue().tiles.map(t => t.letter.char)]}`);
    });

    // taps gamebyid api and stores it in an observable wich will be used to track the requested game
    // if it is received the game object can be used in the html file of game with "observableObject | async"
    this.gameObservable = this.gameService.getGameyById(id).pipe(tap((g: GameDto) => {
      const cells = [];
      for (let x = 0; x < 15; x++) {
        cells[x] = [];
        for (let y = 0; y < 15; y++) {
          cells[x][y] = new Cell(x, y, g.board.charAt(x * 15 + y), 0, environment.multipliers[x][y]);
        }
      }

      // update cells on page load
      this.cellsSubject.next(cells);

      // update current player on page load
      this.currentlyPlayingSubject.next(g.currentlyPlaying);

      // update scores on page load
      const scores = g.scores.map(s => new Score(s.gameId, s.username, s.score));
      this.scoresSubject.next(scores);

      this.game = new Game(
        g.id,
        cells,
        g.players.map(p => new Player(p.username, p.email, this.gameService.dataURItoBlob(p.avatar))),
        new Player(g.creator.username, g.creator.email, this.gameService.dataURItoBlob(g.creator.avatar)),
        g.currentlyPlaying,
        g.over,
        g.hasStarted,
        g.title,
        g.scores.map(s => new Score(s.gameId, s.username, s.score)));
    }));

    // websocket listens to incoming board changes
    this.initializeGameConnection(id);
  }

  private initializeGameConnection(id: string) {
    this.rxStompService.watch(`/games/receive/${id}`)
      .subscribe((message) => {
        const gameDTO: GameDto = JSON.parse(message.body);
        console.log('--------------------\nparsed to gameDTO:');
        console.log(gameDTO);

        // update currently playing
        this.currentlyPlayingSubject.next(gameDTO.currentlyPlaying);

        // update board
        const cells = [];
        for (let x = 0; x < 15; x++) {
          cells[x] = [];
          for (let y = 0; y < 15; y++) {
            cells[x][y] = new Cell(x, y, gameDTO.board.charAt(x * 15 + y), 0, environment.multipliers[x][y]);
          }
        }
        this.cellsSubject.next(cells); // or "this.game.cells = cells;"

        // update scores
        const scores = gameDTO.scores.map(s => new Score(s.gameId, s.username, s.score));
        this.scoresSubject.next(scores);
      }, error => this.error = error.status);

    this.rxStompService.watch(`/games/end/receive/${this.authService.getUserName()}`)
      .subscribe(message => {

        const gameDTO: GameDto = JSON.parse(message.body);
        this.isoverSubject.next(true);
        this.winnerSubject.next(gameDTO.winner);
      });

  }

  /**
   * onConfirmButtonClicked() will send the placed word to the backend to be validated and registered
   * @author jonathan peers
   */
  onConfirmButtonClicked(): void {

    // map object array to dtos
    const laidDownTileDTOS = this.placedTiles
      .map(t =>
        new LaidDownTileDTO(t.x, t.y, t.letter.char));

    console.log(`sending these tiles to backend:
    ${[...laidDownTileDTOS.map(t => t.char)]}
    ${[...laidDownTileDTOS.map(t => `x:${t.x}` + `,y:${t.y}`)]}`);

    // send tiles to backend
    this.gameService.confirmBoard(this.game.id, laidDownTileDTOS).pipe(map((t: PlayedTurnDto) => {
      console.log(`playedTurn Result:
      ${t.board}
      ${t.representation}
      ${[...t.words.map(w => w.word)]}`);

      // send tiles to websocket topic
      this.rxStompService.publish(
        {
          destination: `/games/${this.game.id}/tiles/send`
          , body: JSON.stringify(
            {
              tiles: laidDownTileDTOS,
              gameId: this.game.id,
              username: this.authService.getUserName()
            })
        });

      // update amount of tiles, will be executed in the following (chain)request
      const amountOfNewTiles = this.placedTiles.length;
      return this.rackService.fillLetterRack(this.game.id, amountOfNewTiles);
    })).subscribe(lr$ => lr$.subscribe((lr: LetterRackDto) => {
      console.log(`updated letterRack to: ${[...lr.tiles.map(t => t.char)]}`);
      const tiles = lr.tiles
        .map(c => new Tile(
          new Letter(c.char, 'en_US', this.tileUrls[c.char]),
          null,
          0,
          0,
          c.id));

      // clear rack and fill behaviorsubject with updated rack
      this.rackSubject.next(null);
      this.rackSubject.next(new LetterRack(new Player(lr.player), []));
      tiles.map(t => this.rackSubject.getValue().tiles.push(t));

      // clear placedTiles so the next word will not use the tiles of a previous turn
      this.placedTiles = [];
    }), error1 => {
      console.log(`(confirm word error) ${JSON.stringify(error1)}`);
      this.error = error1.error.message + ', try again!';
    });
  }

  /**
   * this method will receive the onDrop data from the board (childcomponent)
   * the tile will be checked and sorted on x & y coordinates inside a wordarray
   *
   * sometimes we need to readjust the tile on the board itself
   * so we need to delete the old version of the tile in the collection with a matching id
   *
   * @param $event: tile object that will be checked and sorted on x & y coordinates inside a wordarray
   * @author jonathan peers
   */
  formWordEvent($event: Tile) {
    const tile = this.placedTiles.filter(t => t.id === $event.id)[0];
    if (tile != null) {
      console.log(`tile with same id already inside wordArray: ${tile.letter.char} with id ${tile.id}`);
      const index = this.placedTiles.indexOf(tile);
      this.placedTiles.splice(index, 1);
    }

    // add new placedTile and sort the collection
    this.placedTiles.push($event);
    console.log('before sort');
    console.log(`wordtile: ${[...this.placedTiles.map(t => t.letter.char)]}`);
    this.placedTiles.sort((a, b) => a.y - b.y);
    this.placedTiles.sort((a, b) => a.x - b.x);
    console.log('after sort');
    console.log(`wordtile: ${[...this.placedTiles.map(t => t.letter.char)]}`);
  }

  onClearPlacedTiles() {
    // put placed tiles back in rack
    this.placedTiles.map(pt => this.rackSubject.getValue().tiles.push(pt));

    // clear board
    this.placedTiles.map(pt => {
      this.cellsSubject.getValue()[pt.y][pt.x] = new Cell(pt.y, pt.x, '_', 0, environment.multipliers[pt.y][pt.x]);
    });
    this.placedTiles = [];
    this.error = '';
  }

  drag(event, tile: Tile): void {
    // event.dataTransfer.setData('tile', event.target.id);
    console.log(`im dragging tile with id ${tile.id} and char ${tile.letter.char}`);
    event.dataTransfer.setData('tile', JSON.stringify(tile));
  }

  getTileId(tile: Tile): string {
    return JSON.stringify(tile);
  }
}
