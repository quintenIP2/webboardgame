import {PlayerDTO} from './player-dto';
import {ScoreDto} from './score-dto';


export class GameDto {
  id: string;
  creator: PlayerDTO;
  language: string;
  title = 'Scrabble Game';
  players: PlayerDTO[];
  scores: ScoreDto[];
  board: string;
  currentlyPlaying: number;
  over: boolean;
  hasStarted: boolean;
  winner: string;

  constructor(players: Array<PlayerDTO>, gameName: string, language: string) {
    this.players = players;
    this.title = gameName;
    this.language = language;
  }
}
