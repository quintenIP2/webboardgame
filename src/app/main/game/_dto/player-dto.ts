import {Score} from '../_models/score';
import {Game} from '../_models/game';

export class PlayerDTO {
  username: string;
  email: string;
  avatar: string;
  activated: boolean;
  games: Game[];
  scores: Score[];

  constructor(username?: string, email?: string, games?: Game[], scores?: Score[]) {
    this.email = email;
    this.username = username;
    this.games = games;
    this.scores = scores;
  }
}
