export class WordScoreDto {
  word: string;
  score: number;
}
