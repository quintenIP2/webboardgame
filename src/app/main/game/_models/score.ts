export class Score {
  gameId: string;
  username: string;
  score = 0;

  constructor(gameId: string, username: string, score: number) {
    this.gameId = gameId;
    this.username = username;
    this.score = score;
  }
}
