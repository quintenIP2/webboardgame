import {SafeUrl} from '@angular/platform-browser';

export class Player {
  username: string;
  email: string;
  avatarBlob: SafeUrl;
  score = 0;

  constructor(username: string, email?: string, avatarBlob?: SafeUrl) {
    this.username = username;
    this.email = email;
    this.avatarBlob = avatarBlob;
  }
}

