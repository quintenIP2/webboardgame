export class MessageDto {
  name: string;
  content: string;
  gameId: string;

  constructor(name: string, content: string, gameId: string) {
    this.name = name;
    this.content = content;
    this.gameId = gameId;
  }
}
