export class NotificationDTO {
  id: string;
  date: string;
  content: string;
  notifTitle: string;
  gameId: string;

  constructor(content: string, title: string, date: string, gameId: string, id: string) {
    this.content = content;
    this.notifTitle = title;
    this.date = date;
    this.gameId = gameId;
    this.id = id;
  }
}
