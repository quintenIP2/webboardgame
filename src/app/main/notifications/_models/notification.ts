export class Notification {
  id: string;
  date: Date;
  content: string;
  notifTitle: string;
  gameId: string;

  constructor(content: string, title: string, date: Date, gameId: string, id: string) {
    this.content = content;
    this.notifTitle = title;
    this.date = date;
    this.gameId = gameId;
    this.id = id;
  }
}
