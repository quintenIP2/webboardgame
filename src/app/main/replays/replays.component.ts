import {Component, OnInit} from '@angular/core';
import {GameOverviewDTO} from '../overview/_dto/game-overview-dto';
import {Player} from '../game/_models/player';
import {GameOverview} from '../overview/_models/game-overview';
import {GameService} from '../_services/game.service';
import {Router} from '@angular/router';


/**
 * component used to display an overview of replays
 */
@Component({selector: 'app-replays', templateUrl: './replays.component.html', styleUrls: ['./replays.component.scss']})
export class ReplaysComponent implements OnInit {
  games: GameOverview[] = [];

  constructor(private gameService: GameService,
              private router: Router) {
  }

  ngOnInit() {
    this.gameService.getAllGames().subscribe((data: Array<GameOverviewDTO>) => {
      data.map(g => {
        const creator = new Player(
          g.creator.username,
          g.creator.email,
          this.gameService.dataURItoBlob(g.creator.avatar));
        const players = g.players.map(p =>
          new Player(p.username, p.email, this.gameService.dataURItoBlob(p.avatar)));

        this.games.push(new GameOverview(g.id, g.title, g.currentlyPlaying, players, creator, g.over, g.hasStarted, g.turns, g.winner));
      });
    });
  }


  onSelectReplayGame(id: any) {
    // get replay of game with restcall
    this.router.navigate([`/replays/${id}`]);
  }
}
