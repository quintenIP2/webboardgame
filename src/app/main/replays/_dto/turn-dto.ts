import {PlayerDTO} from '../../game/_dto/player-dto';

export class TurnDto {
  gameId: string;
  turnCounter: number;
  board: string;
  player: string;
  score: number;
  racks: string;
  users: PlayerDTO[];
}
