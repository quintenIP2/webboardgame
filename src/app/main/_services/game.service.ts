import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {GameDto} from '../game/_dto/game-dto';
import {DomSanitizer} from '@angular/platform-browser';
import {NewGameDto} from '../newgame/_dto/new-game-dto';
import {LaidDownTileDTO} from '../game/_dto/laidDownTileDTO';
import {PlaceTilesRequestDto} from '../game/_dto/place-tiles-request-dto';


/**
 * service used to handle all the things related to playing a game
 *
 * @getGameById: fetches a game by id
 * @getAllAcceptedGames: fetches all the games where the invitations of the logged in player have been accepted
 * @deleteGameById: deletes a game by id
 * @confirmBoard: confirms the played tiles of a player during a turn. The layed tiles will be sorted and sent to the backend.
 * @getAllTurnsByGameId: gets all the turns that will be used to show the history of the game in the replaycomponent
 * @updateInvitation: sends the response to an invitation to the server.
 * If the player has accepted to join a game it will be added to his overview
 * @dataURItoBlob: transforms a base64 string to a blob object
 */
@Injectable({providedIn: 'root'})
export class GameService {

  constructor(private http: HttpClient, private domSanitizer: DomSanitizer) {
  }

  createNewGame(gameName: string, players: Array<string>): Observable<GameDto> {
    const requestBody = new NewGameDto(gameName, 'en', players);
    return this.http.post<GameDto>(`${environment.baseUrl}/api/games`, requestBody);
  }

  // can be used to manually start a game
  startGame(gameId: string): Observable<Boolean> {
    return this.http.post<Boolean>(`${environment.baseUrl}/api/games/${gameId}`, '');
  }

  getAllGames(): Observable<any> {
    return this.http.get(`${environment.baseUrl}/api/overview`);
  }

  getAllAcceptedGames(): Observable<any> {
    return this.http.get(`${environment.baseUrl}/api/overview/accepted`);
  }

  getGameyById(id: String): Observable<any> {
    return this.http.get(`${environment.baseUrl}/api/games/${id}`);
  }

  deleteGameById(id: String): Observable<any> {
    return this.http.delete(`${environment.baseUrl}/api/games/${id}`);
  }

  confirmBoard(gameId: string, tiles: Array<LaidDownTileDTO>): Observable<any> {
    return this.http.post(`${environment.baseUrl}/api/games/${gameId}/tiles`, new PlaceTilesRequestDto(tiles));
  }

  getAllTurnsByGameId(gameId: string): Observable<any> {
    return this.http.get(`${environment.baseUrl}/api/games/${gameId}/turns`);
  }

  updateInvitation(gameId: String, hasAccepted: boolean): Observable<any> {
    const header = new HttpHeaders().set('Content-Type', 'application/json');

    return this.http.post(`${environment.baseUrl}/api/games/${gameId}/invitations`,
      JSON.stringify({hasAccepted: hasAccepted}),
      {headers: header});
  }

  // converts base64 to image blobs
  dataURItoBlob(dataURI) {
    const byteString = atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);

    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }

    return this.domSanitizer.bypassSecurityTrustUrl(
      URL.createObjectURL(new Blob([arrayBuffer], {type: 'image/png'})));
  }
}

