import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

/**
 * service used to handle all the things related to fetching and deleting notifications of the logged in user
 */
 @Injectable({providedIn: 'root'})
export class NotificationService {

  constructor(private http: HttpClient) {
  }

  getMyNotifications(): Observable<any> {
    return this.http.get(`${environment.baseUrl}/api/notifications`);
  }

  deleteNotification(notifId: String) {
    return this.http.delete(`${environment.baseUrl}/api/notifications/${notifId}`);
  }
}
