import {Component, Inject, OnInit} from '@angular/core';
import {FriendsService} from '../_services/friends.service';
import {PlayerDTO} from '../game/_dto/player-dto';
import {Player} from '../game/_models/player';
import {GameService} from '../_services/game.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable, of} from 'rxjs';



/**
 * component to handle everything that has to do with friends
 */
@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.scss']
})
export class FriendsComponent implements OnInit {
  friends: Array<Player> = [];
  addFriendForm = new FormGroup({});
  errorMessage: Observable<string>;
  succesResponse: Observable<string>;

  constructor(private friendService: FriendsService,
              private gameService: GameService,
              @Inject(FormBuilder) private fb: FormBuilder) {
  }

  ngOnInit() {
    this.addFriendForm = this.fb.group({username: ['', [Validators.required]]});

    this.friendService.getAllFriends()
      .subscribe((friends: Array<PlayerDTO>) => {
        friends.map(f => {
          const p = new Player(f.username, f.email, this.gameService.dataURItoBlob(f.avatar));
          this.friends.push(p);
        });
      });
  }

  get form() {
    return this.addFriendForm.controls;
  }

  onAddFriend() {
    this.errorMessage = of('');
    this.succesResponse = of('');

    if (this.addFriendForm.valid) {
      this.friendService.addFriend(this.form.username.value).subscribe(value => {
        console.log(`response: ${JSON.stringify(value)}`);
        this.succesResponse = of('User is added successfully');
      }, err => {
        console.log(err.status);
        switch (JSON.parse(err.status)) {
          case 401:
            this.errorMessage = of('User does not exist');
            break;
          case 400:
            this.errorMessage = of('User is already your friend');
            break;

        }
      });
    }
  }
}
