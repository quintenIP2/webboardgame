import {GameOverview} from './game-overview';

export class SubOverview {
  games: Array<GameOverview>;
  type: string;

  constructor(games: Array<GameOverview>, title: string) {
    this.games = games;
    this.type = title;
  }
}
