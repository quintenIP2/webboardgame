import {tileUrls} from './tiles';
import {multipliers} from './multipliers';

export const environment = {
  production: true,
  baseUrl: 'https://team-bitconnect-web.herokuapp.com/',
  currentUser: 'user_data',
  clientid: 'devglan-client',
  tileUrls: tileUrls,
  multipliers: multipliers,
  clientsecret: 'devglan-secret',
  localStorageToken: 'access_token',
  facebookClientId: '2030081433960714',
  googleClientId: '503629548222-49qud7tng2ei0slggf3fmt499d2qmhoq.apps.googleusercontent.com',
  starUrl: 'https://www.rockyknobwoodcrafts.com/prodimages/Scrabble%20Star_small.JPG',
  doubleLetterScoreUrl: 'https://europeisnotdead.files.wordpress.com/2012/09/european-longest-words-double-letter-score.png',
  tripleLetterScoreUrl: 'https://europeisnotdead.files.wordpress.com/2012/09/european-longest-words-triple-letter-score.png',
  doubleWordScoreUrl: 'https://europeisnotdead.files.wordpress.com/2012/09/european-longest-words-double-word-score.png',
  tripleWordScoreUrl: 'https://europeisnotdead.files.wordpress.com/2012/09/european-longest-words-triple-word-score1.png'
};

// https://team-bitconnect-web.herokuapp.com/
