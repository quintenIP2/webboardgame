// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import {tileUrls} from './tiles';
import {multipliers} from './multipliers';

export const environment = {
  production: false,
  baseUrl: 'http://localhost:8080',
  // baseUrl: 'https://team-bitconnect-backend.herokuapp.com',
  currentUser: 'user_data',
  tileUrls: tileUrls,
  multipliers: multipliers,
  clientid: 'devglan-client',
  clientsecret: 'devglan-secret',
  localStorageToken: 'access_token',
  facebookClientId: '2030081433960714',
  googleClientId: '503629548222-49qud7tng2ei0slggf3fmt499d2qmhoq.apps.googleusercontent.com',
  starUrl: 'https://www.rockyknobwoodcrafts.com/prodimages/Scrabble%20Star_small.JPG',
  doubleLetterScoreUrl: 'https://europeisnotdead.files.wordpress.com/2012/09/european-longest-words-double-letter-score.png',
  tripleLetterScoreUrl: 'https://europeisnotdead.files.wordpress.com/2012/09/european-longest-words-triple-letter-score.png',
  doubleWordScoreUrl: 'https://europeisnotdead.files.wordpress.com/2012/09/european-longest-words-double-word-score.png',
  tripleWordScoreUrl: 'https://europeisnotdead.files.wordpress.com/2012/09/european-longest-words-triple-word-score1.png'
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
